package controllers;

import models.SecurityRole;
import models.dto.MediatorsEmailBean;
import org.apache.commons.lang3.StringUtils;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.ctrl.CtrlUtils;
import utils.email.Sender;

/**
 * Created by HeavyPollo on 5/15/16.
 */
public class MediatorsCtrl extends Controller{
    private static final String Module = "mediators";

    public static Result sendEmail() {

        Form<MediatorsEmailBean> form = Form.form(MediatorsEmailBean.class)
                .bindFromRequest();

        if (form.hasErrors()) {
            return CtrlUtils.responseWithErrors(form, Module);
        }else {
            MediatorsEmailBean dto = form.get();
            String[] emails = StringUtils.split(dto.getMediatorEmail(),",");
            boolean result = Sender.sendEmail("Petición de Mediación",
                    views.html.email.mediatorsEmail.render(dto).toString(),
                    emails
            );
            if (result) return ok(); else return internalServerError();
        }
    }

}
