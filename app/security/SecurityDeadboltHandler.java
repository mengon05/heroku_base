package security;

import be.objectify.deadbolt.core.models.Subject;
import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import models.User;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.util.Optional;

/**
 * Created by Interax on 13/01/2016.
 */
public class SecurityDeadboltHandler extends AbstractDeadboltHandler {

    public static final String AUTH_AUTHORIZATION = "Authorization";
    public static final String CONTEXT_USER = "context_user";
    @Override
    public F.Promise<Optional<Result>> beforeAuthCheck(Http.Context context) {
        if(getAuthorizationToken(context) != null){
            return F.Promise.promise(() -> Optional.ofNullable((Result) null));
        }else{
            return F.Promise.promise(() -> Optional.ofNullable((Result) Controller.forbidden()));
        }
    }

    @Override
    public F.Promise<Optional<Subject>> getSubject(Http.Context context) {
        User user ;
        if(context.args.get(CONTEXT_USER) != null){
            user = (User)context.args.get(CONTEXT_USER);
            return F.Promise.promise(() -> Optional.ofNullable(user));
        }
        String authTokenHeaderValue = getAuthorizationToken(context);
        if (authTokenHeaderValue != null) {
            user = models.SecurityToken.findByToken(authTokenHeaderValue).getUser();
            if (user != null) {
                context.args.put(CONTEXT_USER, user);
                return F.Promise.promise(() -> Optional.ofNullable(user));
            }
        }
        return F.Promise.promise(() -> Optional.empty());
    }

    @Override
    public F.Promise<Result> onAuthFailure(Http.Context context, String s) {
        return F.Promise.pure((Result) Controller.unauthorized());
    }

    private String getAuthorizationToken(Http.Context context) {
        String[] authTokenHeaderValues = context.request().headers().get(AUTH_AUTHORIZATION.toUpperCase());
        if (authTokenHeaderValues == null) {
            authTokenHeaderValues = context.request().headers().get(AUTH_AUTHORIZATION);
        }
        if (authTokenHeaderValues == null) {
            return null;
        }
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)){
            return authTokenHeaderValues[0];
        }else{
            return null;
        }
    }
}