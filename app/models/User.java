package models;

import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Subject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import utils.json.View;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;


/**
 *Created by Pmendoza on 13/01/2016.
 */
@Entity
public class User extends BaseEntity implements Subject{

    @Id
    @JsonView(View.Public.class)
    private Long id;

    @Column(length = 256, unique = true, nullable = false)
    @Constraints.Required
    @JsonView(View.Public.class)
    private String userName;

    @Column(length = 256,nullable = false)
    @Constraints.Required
    @JsonView(View.Public.class)
    private String email;


    @Column(length = 256, nullable = false)
    @JsonView(View.Public.class)
    private String givenName;

    @Column(length = 256, nullable = false)
    @JsonView(View.Public.class)
    private String familyName;

    @ManyToMany
    @JsonView(View.UserForm.class)
    private List<SecurityRole> roles = new ArrayList<SecurityRole>();

    @Column(length = 64, nullable = false)
    @JsonIgnore
    private byte[] shaPassword;

    @Transient
    @JsonIgnore
    private String password;

    public User() {

    }


    public User(String email, String password, String name, String lasName) {
        setEmail(email);
        setPassword(password);
        this.givenName = name;
        this.familyName = lasName;
        String[] args = StringUtils.split(email,"@");
        if(args.length > 0){
            setUserName(args[0]);
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        shaPassword = getSha512(password);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRoles(List<SecurityRole> roles) {
        this.roles = roles;
    }

    public List<? extends SecurityRole> getRoles() {
        return roles;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    //-----------------------------------------------------------------------------
    @JsonIgnore
    public List<? extends Permission> getPermissions() {
        List<Permission> permissions = new ArrayList<Permission>();
        for(SecurityRole role : roles){
            for(SecurityPermission permission : role.getPermissions()){
                permissions.addAll(permission.getDeadboltPermission());
            }
        }
        return permissions;

    }

    @Override
    @JsonIgnore
    public String getIdentifier() {
        return String.valueOf(id);
    }

    public static final Finder<Long, User> find = new Finder<Long, User>(Long.class,User.class);

    public static byte[] getSha512(String value) {
        try {
            return MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"));
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static User findByEmailAndPassword(String email, String password) {
        return find.where()
                .eq("email", email)
                .eq("shaPassword", getSha512(password))
                .findUnique();
    }

    public static User findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }

    public static Map<String,Set<String>> getExcludedActionsAsMap(User user){
        /**
         * Map<Permission,ExcludedActions>
         */
        Map<String,Set<String>> permissions = new HashMap<String,Set<String>>();
        for(SecurityRole role : user.getRoles()){
            for(SecurityPermission permission : role.getPermissions()){
                Set<String> previousExcludedActions = permissions.get(permission.getModule().getId());
                Set<String> excludedActions = new HashSet<String>();
                for(SecurityAction action : permission.getExcludedActions()){
                    excludedActions.add(action.getId());
                }
                Set<String> mergedActions = new HashSet<String>();
                /**
                 * Convina Array 1 y Array 2, solo se queda los elementos que se repitan en ambos
                 */
                if(previousExcludedActions != null){
                    for(String action : previousExcludedActions){
                        if(excludedActions.contains(action)){
                            mergedActions.add(action);
                        }
                    }
                }else{
                    mergedActions = excludedActions;
                }
                permissions.put(permission.getModule().getId(),mergedActions);
            }
        }
        return permissions;
    }

}
