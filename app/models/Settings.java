package models;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import play.Logger;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Interax on 11/02/2016.
 */
@Entity
public class Settings extends BaseEntity{


    public static final String EMAIL_ALIAS = "mail.alias";
    public static final String EMAIL_MOCK = "mail.mock";
    @Id
    private String id;

    @Column
    private String value;

    @Column (name = "GROUP_TYPE")
    @Enumerated(EnumType.STRING)
    private Group group;

    @Column
    @Enumerated(EnumType.STRING)
    private Type type = Type.STRING;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    /**
     * Add types whereever you need to classify the settings
     */
    public enum Group {
        EMAIL,
        GENERAL,
    }

    /**
     * Add types whereever you need to classify the settings
     */
    public enum Type {
        BOOLEAN,
        STRING,
        NUMBER
    }

    public static Finder<String, Settings> find = new Finder(String.class,Settings.class);

    public static String getString(String settingName){
        Settings st = Settings.find.byId(settingName);
        if(st == null){
            return "";
        }else{
            return st.getValue();
        }
    }

    public static boolean getBoolean(String settingsName){
        Settings st = Settings.find.byId(settingsName);
        if(st == null){
            return false;
        }else{
            return "true".equalsIgnoreCase(st.getValue());
        }
    }
    public static List<Settings> getSettingsByGroup(Group group){
        return find.where().eq("group", group).findList();
    }

    public static Integer getInteger(String settingName){
        Settings st = Settings.find.byId(settingName);
        if(st == null){
            return 0;
        }else{
            try{
                return Integer.parseInt(st.getValue());
            }catch (Exception e){
                Logger.error("",e);
                return 0;
            }

        }
    }
    public static Double getDouble(String settingName){
        Settings st = Settings.find.byId(settingName);
        if(st == null){
            return 0d;
        }else{
            try{
                return Double.parseDouble(st.getValue());
            }catch (Exception e){
                Logger.error("",e);
                return 0d;
            }

        }
    }
}
