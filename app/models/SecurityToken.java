package models;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.persistence.*;
import javax.xml.bind.DatatypeConverter;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * Created by Interax on 22/02/2016.
 */
@Entity
@Table(name ="s_token")
public class SecurityToken extends BaseEntity{

    @Id
    private long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private SecurityApp app;

    @Column
    private String token;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SecurityApp getApp() {
        return app;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void createToken(){
        int keyLen = 128;
        KeyGenerator keyGen = null;
        try {
            keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(keyLen);
            SecretKey secretKey = keyGen.generateKey();
            byte[] encoded = secretKey.getEncoded();
            token = DatatypeConverter.printHexBinary(encoded).toLowerCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            token = UUID.randomUUID().toString();
        }
        save();
    }



    public static SecurityToken findByToken(String token){
        return find.fetch("user").where().eq("token",token).findUnique();
    }

    public void setApp(SecurityApp app) {
        this.app = app;
    }
    public static Finder<Long, SecurityToken> find = new Finder<Long, SecurityToken>(Long.class,SecurityToken.class);

    public static SecurityToken createToken(User user, SecurityApp app){
        SecurityToken token = SecurityToken.find.where()
                .eq("user",user)
                .eq("app",app)
                .findUnique();
        if(token == null){
            token = new SecurityToken();
            token.setApp(app);
            token.setUser(user);
            token.setCreatedBy(user);
            token.setUpdatedBy(user);
        }
        token.createToken();
        return token;
    }


}
