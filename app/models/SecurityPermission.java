package models;

import be.objectify.deadbolt.core.models.Permission;
import com.fasterxml.jackson.annotation.JsonView;
import utils.EntityUtils;
import utils.json.View;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 *Created by Pmendoza on 13/01/2016.
 */
@Entity
@Table(name = "s_permission",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"role_id","module_id"})
)

public class SecurityPermission extends BaseEntity {
    @Id
    @JsonView(View.RoleForm.class)
    private long id;

    @ManyToOne
    private SecurityRole role;

    @ManyToOne
    @JsonView(View.RoleForm.class)
    private SecurityModule module;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonView(View.RoleForm.class)
    private List<SecurityAction> excludedActions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SecurityRole getRole() {
        return role;
    }

    public void setRole(SecurityRole role) {
        this.role = role;
    }

    public SecurityModule getModule() {
        return module;
    }

    public void setModule(SecurityModule module) {
        this.module = module;
    }

    public List<SecurityAction> getExcludedActions() {
        return excludedActions;
    }

    public void setExcludedActions(List<SecurityAction> excludedActions) {
        this.excludedActions = excludedActions;
    }

    public List<? extends Permission> getDeadboltPermission(){
        List<DeadboltPermission> deadboltPermissions = new ArrayList<DeadboltPermission>();
        deadboltPermissions.add(new DeadboltPermission(module.getId()));
        for(SecurityAction actions : module.getActions()){
            if(!excludedActions.contains(actions)){
                deadboltPermissions.add(new DeadboltPermission(module.getId() + "." + actions.getId()));
            }
        }
        return deadboltPermissions;
    }

    public class DeadboltPermission implements Permission{
        private String value;
        public DeadboltPermission(String value){
            this.value = value;
        }
        @Override
        public String getValue() {
            return value;
        }
    }

    public static Finder<Long, SecurityPermission> find = new Finder(Long.class,SecurityPermission.class);

}
