package models.dto;

import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.validation.Constraints;
import utils.DateUtils;

import java.util.Date;

/**
 * Created by HeavyPollo on 5/15/16.
 */
public class MediatorsEmailBean {
    @Constraints.Required
    private String name;
    private String phone;
    private String email;
    @Constraints.Required
    private String conflict;
    @Constraints.Required
    private String facts;
    @Constraints.Required
    private String mediatorName;
    @Constraints.Required
    private String mediatorEmail;
    private String where;
    private boolean inOffice;
    private long when;
    private boolean invite;

    private String invitedName;
    private String invitedAddress;
    private boolean sendInvitation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFacts() {
        return facts;
    }

    public void setFacts(String facts) {
        this.facts = facts;
    }

    public String getMediatorName() {
        return mediatorName;
    }

    public void setMediatorName(String mediatorName) {
        this.mediatorName = mediatorName;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public boolean isInOffice() {
        return inOffice;
    }

    public void setInOffice(boolean inOffice) {
        this.inOffice = inOffice;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }

    public String getInvitedName() {
        return invitedName;
    }

    public void setInvitedName(String invitedName) {
        this.invitedName = invitedName;
    }

    public String getInvitedAddress() {
        return invitedAddress;
    }

    public void setInvitedAddress(String invitedAddress) {
        this.invitedAddress = invitedAddress;
    }

    public boolean isSendInvitation() {
        return sendInvitation;
    }

    public void setSendInvitation(boolean sendInvitation) {
        this.sendInvitation = sendInvitation;
    }

    public String getConflict() {
        return conflict;
    }

    public void setConflict(String conflict) {
        this.conflict = conflict;
    }

    public String getWhenStr(){
        if(getWhen() > 1){
            try{
                return DateUtils.SDF.DDMMYYY_24HMM.format(new Date(getWhen()));
            }catch (Exception e){
                Logger.warn(String.format("Error al transformar fecha en milisegundos %d", getWhen()));
                return NOT_AVAILABLE;
            }
        }else{
            return NOT_AVAILABLE;
        }
    }

    public String getInvitedNameFixed(){
       if(StringUtils.isBlank(getInvitedName())){
           return NA;
       }else{
           return getInvitedName();
       }
    }
    public String getInvitedAddressFixed(){
        if(StringUtils.isBlank(getInvitedAddress())){
            return NA;
        }else{
            return getInvitedAddress();
        }
    }
    public String getSendInvitationStr(){
        if(NA.equals(getInvitedNameFixed())){
            return NA;
        }else{
            if(isSendInvitation()){
                return "Si";
            }else{
                return "No";
            }
        }
    }

    public String getMediatorEmail() {
        return mediatorEmail;
    }

    public void setMediatorEmail(String mediatorEmail) {
        this.mediatorEmail = mediatorEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public boolean isInvite() {
        return invite;
    }

    public void setInvite(boolean invite) {
        this.invite = invite;
    }

    public static final String NA = "N/A";
    public static final String NOT_AVAILABLE = "No disponible";
}
