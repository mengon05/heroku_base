package utils;

import java.text.SimpleDateFormat;

/**
 * Created by HeavyPollo on 5/15/16.
 */
public class DateUtils {

    public static class SDF{
        public static final SimpleDateFormat DDMMYYY_24HMM = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    }
}
