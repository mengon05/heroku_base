package models.dto;

import org.junit.Test;
import utils.DateUtils;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by HeavyPollo on 5/15/16.
 */
public class MediatorsEmailBeanTest {

    @Test
    public void getWhenStr() throws Exception {
        MediatorsEmailBean bean = new MediatorsEmailBean();

        assertEquals("No disponible", bean.getWhenStr());
        bean.setWhen(1L);
        assertEquals("No disponible", bean.getWhenStr());
        bean.setWhen(10L);
        assertEquals(bean.getWhenStr(), DateUtils.SDF.DDMMYYY_24HMM.format(new Date(bean.getWhen())));
        bean.setWhen(System.currentTimeMillis());
        assertEquals(bean.getWhenStr(), DateUtils.SDF.DDMMYYY_24HMM.format(new Date(bean.getWhen())));
    }

    @Test
    public void getInvitedStr() throws Exception {
        MediatorsEmailBean bean = new MediatorsEmailBean();
        bean.setInvitedName(null);
        assertEquals("N/A", bean.getInvitedNameFixed());
        bean.setInvitedName("");
        assertEquals("N/A", bean.getInvitedNameFixed());
        bean.setInvitedName(" ");
        assertEquals("N/A", bean.getInvitedNameFixed());
        bean.setInvitedName("   ");
        assertEquals("N/A", bean.getInvitedNameFixed());
        bean.setInvitedName("Junit Test");
        assertEquals("Junit Test", bean.getInvitedNameFixed());

        bean.setInvitedAddress(null);
        assertEquals("N/A", bean.getInvitedAddressFixed());
        bean.setInvitedAddress("");
        assertEquals("N/A", bean.getInvitedAddressFixed());
        bean.setInvitedAddress(" ");
        assertEquals("N/A", bean.getInvitedAddressFixed());
        bean.setInvitedAddress("   ");
        assertEquals("N/A", bean.getInvitedAddressFixed());
        bean.setInvitedAddress("Junit Test");
        assertEquals("Junit Test", bean.getInvitedAddressFixed());

    }

    @Test
    public void testSendInvitationStr(){
        MediatorsEmailBean bean = new MediatorsEmailBean();
        bean.setInvitedName(null);

        assertEquals("N/A", bean.getSendInvitationStr());

        bean.setInvitedName("Junit test");
        assertEquals("No", bean.getSendInvitationStr());
        bean.setSendInvitation(true);
        assertEquals("Si", bean.getSendInvitationStr());

    }

}