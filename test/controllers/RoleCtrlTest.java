package controllers;


/**
 * Created by Interax on 16/02/2016.
 */
public class RoleCtrlTest  {
//    @Test
//    public void getAllTest() {
//        String order = "name";
//        Integer limit = 10;
//        Integer page = 0;
//        String search = "";
//        List<String> fields = new ArrayList<>();
//        Http.RequestBuilder rb = Helpers.fakeRequest(routes.RoleCtrl.getAll(order,limit,page,search,fields));
//        Result result = route(rb);
//        assertEquals(OK, result.status());
//    }
//    @Test
//    public void getTest() {
//        Http.RequestBuilder rb = Helpers.fakeRequest(routes.RoleCtrl.get(1));
//        Result result = route(rb);
//        assertEquals(OK, result.status());
//
//        rb = Helpers.fakeRequest(routes.RoleCtrl.get(99999));
//        result = route(rb);
//        assertEquals(NOT_FOUND, result.status());
//    }
//
//    @Test
//    public void postTest() {
//        Http.RequestBuilder rb = Helpers.fakeRequest(routes.RoleCtrl.post());
//        String roleName = "Junit role";
//        SecurityRole role = new SecurityRole();
//        role.setName(roleName);
//        role.setPermissions(addAllPermissionsAllExcluded(role));
//        JsonNode node = Json.parse(JsonUtils.fromJsonView(role, View.RoleForm.class));
//        rb.bodyJson(node);
//        Result result = route(rb);
//        //Verificar respuesta satisfactoria
//        assertEquals(OK, result.status());
//        SecurityRole saved = SecurityRole.find.where().eq("name",roleName).findUnique();
//        //Verificar que el rol se haya insertado en DB
//        assertNotNull(saved);
//        //Verificar que tenga los mismos permisos que los que enviamos en la peticion
//        assertEquals(role.getPermissions().size(), saved.getPermissions().size());
//        for(SecurityPermission perm : saved.getPermissions()){
//            //Verificar que tambien se haya guardado las acciones excluidas
//            assertFalse(perm.getExcludedActions().isEmpty());
//        }
//    }
//
//    private static List<SecurityPermission> addAllPermissionsAllExcluded(SecurityRole role ){
//        List<SecurityPermission> permissions = new ArrayList<>();
//        for(SecurityModule module : SecurityModule.find.fetch("actions").findList()){
//            SecurityPermission perm = new SecurityPermission();
//            perm.setModule(module);
//            perm.setRole(role);
//            perm.setExcludedActions(module.getActions());
//            permissions.add(perm);
//        }
//        return permissions;
//    }
//    @Test
//    public void putTest() {
//
//        String roleName = "Junit role";
//        SecurityRole original = new SecurityRole();
//        original.setName(roleName);
//        original.setPermissions(addAllPermissionsAllExcluded(original));
//        original.save();
//
//        Http.RequestBuilder rb = Helpers.fakeRequest(routes.RoleCtrl.put());
//
//        SecurityRole modified = SecurityRole.find.where().eq("name",roleName).findUnique();
//        modified.getPermissions().remove(0);
//        JsonNode node = Json.parse(JsonUtils.fromJsonView(modified, View.RoleForm.class));
//        rb.bodyJson(node);
//        Result result = route(rb);
//        //Verificar respuesta satisfactoria
//        assertEquals(OK, result.status());
//        SecurityRole saved = SecurityRole.find
//                .fetch("permissions.excludedActions")
//                .where()
//                .eq("name",roleName).findUnique();
//        //Verificar que el rol se haya insertado en DB
//        assertNotNull(saved);
//        //Verificar que tenga los mismos permisos que los que enviamos en la peticion
//        assertEquals(modified.getPermissions().size(), saved.getPermissions().size());
//        for(SecurityPermission perm : saved.getPermissions()){
//            //Verificar que tambien se haya guardado las acciones excluidas
//            assertFalse(perm.getExcludedActions().isEmpty());
//        }
//    }
}
