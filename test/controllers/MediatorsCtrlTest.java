package controllers;

import models.dto.MediatorsEmailBean;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import play.test.WithApplication;
import utils.email.Sender;

/**
 * Created by HeavyPollo on 9/25/16.
 */
public class MediatorsCtrlTest extends WithApplication {

    @Test
    public void testSendEmail(){
        MediatorsEmailBean bean = new MediatorsEmailBean();
        bean.setEmail("mengon.05@gmail.com");
        bean.setConflict("Conflict test");
        bean.setFacts("Facts test");
        bean.setInOffice(true);
        bean.setInvitedAddress("Invited address test");
        bean.setMediatorEmail("mengon05@gmail.com,men.gon.05@gmail.com");
        bean.setMediatorName("Junit mediator");
        bean.setName("Test name");
        bean.setPhone("Test phone");
        bean.setSendInvitation(true);
        bean.setWhen(new DateTime().getMillis());
        bean.setWhere("Where test");

        String email = views.html.email.mediatorsEmail.render(bean).toString();
        System.out.println(email);
        Assert.assertTrue(email.contains(bean.getEmail()));
        Assert.assertTrue(email.contains(bean.getConflict()));
        Assert.assertTrue(email.contains(bean.getFacts()));
        Assert.assertTrue(email.contains(bean.getInvitedAddress()));
        Assert.assertFalse(email.contains(bean.getMediatorEmail()));
        Assert.assertTrue(email.contains(bean.getMediatorName()));
        Assert.assertTrue(email.contains(bean.getName()));
        Assert.assertTrue(email.contains(bean.getPhone()));
        Assert.assertFalse(email.contains(bean.getWhere()));
        String[] emails = StringUtils.split(bean.getMediatorEmail(),",");

        Sender.sendEmail("Petición de Mediación",
                views.html.email.mediatorsEmail.render(bean).toString(),
                emails
        );
    }

    @Test
    public void testInOfficeLogic(){
        MediatorsEmailBean bean = new MediatorsEmailBean();
        bean.setEmail("mengon.05@gmail.com");
        bean.setConflict("Conflict test");
        bean.setFacts("Facts test");
        bean.setInOffice(true);
        bean.setInvitedAddress("Invited address test");
        bean.setMediatorEmail("mengon05@gmail.com");
        bean.setMediatorName("Junit mediator");
        bean.setName("Test name");
        bean.setPhone("Test phone");
        bean.setSendInvitation(true);
        bean.setWhen(new DateTime().getMillis());
        bean.setWhere("Where test");

        String email = views.html.email.mediatorsEmail.render(bean).toString();
        Assert.assertFalse(email.contains(bean.getWhere()));
        bean.setInOffice(false);
        email = views.html.email.mediatorsEmail.render(bean).toString();
        Assert.assertTrue(email.contains(bean.getWhere()));
    }

    public void testNA(){

    }

}
