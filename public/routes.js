(function(){
    var app = angular.module('app.routes',['ui.router']);
    app.config(function($stateProvider,$urlRouterProvider) {
        $stateProvider.state('app',{
            url : "/app",
            abstract : true,
            controller: "MenuCtrl",
            templateUrl:'app/menu/menu.html',
            onEnter: function(){
                console.log("enter app");
            }
        });
        $stateProvider.state('app.userList',{
            url:'/userList',
            templateUrl:'app/user/user.list.html',
            controller : 'UserListCtrl'
        });
        $stateProvider.state('app.roleList',{
            url:'/roleList',
            templateUrl:'app/role/role.list.html',
            controller : "RoleListCtrl"
        });
        $stateProvider.state('app.settings',{
            url:'/settings',
            templateUrl:'app/settings/settings.html',
            controller : "SettingsCtrl"
        });
        $stateProvider.state('app.apkManagerList',{
            url:'/apkManagerList',
            templateUrl:'app/apk/apk.list.html',
            controller : "ApkManagerListCtrl"
        });
        $urlRouterProvider.otherwise('/app/userList');
    });
}());