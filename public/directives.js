(function(){
    var app  = angular.module('app.common.directives', []);


    app.directive('appToolbar', [function() {
        return {
            templateUrl : 'template/app.toolbar.html',
            scope: {
                selected : '=',
                query : '=',
                onEdit :'&?',
                onDelete :'&?',
                title : '@',
                hideDeleteButton:"=?",
                hideEditButton:"=?"
            },
            link: function(scope, element) {
                scope.action = {
                    edit : angular.isDefined(scope.onEdit) && !scope.hideEditButton,
                    delete : angular.isDefined(scope.onDelete) && !scope.hideDeleteButton
                };
                scope.query.search = '';
                scope._toolbar = {};
                scope._toolbar.showSearch = false;
                scope._toolbar.toggleSearch = function(){
                    scope._toolbar.showSearch = !scope._toolbar.showSearch;
                }
            }
        };
    }]);

    app.directive('minimumVersion', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            scope: {
                minimumVersion: '=',
            },
            require: 'ngModel',
            link: function link(scope, elem, attrs, ctrl) {
                var validator = function (value) {
                    console.log(value < scope.minimumVersion);
                    ctrl.$setValidity('minimumVersion', value >= scope.minimumVersion);
                    return value;
                }

                ctrl.$parsers.unshift(validator);
                ctrl.$formatters.push(validator);

            }
        };
    }]);

    app.directive('passwordConfirm', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            scope: {
                matchTarget: '=',
            },
            require: 'ngModel',
            link: function link(scope, elem, attrs, ctrl) {
                var validator = function (value) {
                    ctrl.$setValidity('match', value === scope.matchTarget);
                    return value;
                }

                ctrl.$parsers.unshift(validator);
                ctrl.$formatters.push(validator);

                // This is to force validator when the original password gets changed
                scope.$watch('matchTarget', function(newval, oldval) {
                    validator(ctrl.$viewValue);
                });

            }
        };
    }]);

}());