(function(){
    var app = angular.module("app.menu", []);


    app.factory("MenuService", function($mdSidenav){
        function close(){
            $mdSidenav('left').close();
        }
        function toggle(){
            $mdSidenav('left').toggle();
        }

        return {
            setupMenu : function(scope,module){
                scope._menu = {module : module};
                scope._menu.close = function () {
                    close();
                };
                scope._menu.toggle = function(){
                    toggle();
                };

            }
        }
    });


    app.controller('MenuCtrl', function ($scope, $timeout, $mdSidenav, $log,AuthService,$localStorage,MenuService,$mdMedia) {
        MenuService.setupMenu($scope);
        $scope.logout = function(){
            AuthService.logout();
        }
        var data =  $localStorage.user;
        var permissions = data ? data.permissions : [];



        /**
         * Guarda la configuracion de la presentacion de la opcion de menu
         * como el icono y el state al que esta ligado
         * @type {{module1: {icon: string, state: string}, module2: {icon: string}}}
         */
        var menuInnerProperties = {
            apkManager : {icon : 'people',state:"app.apkManagerList"},
            user : {icon : 'people',state:"app.userList"},
            role : {icon : 'assignment_ind',state:"app.roleList"},
            settings : {icon : 'settings',state:"app.settings"}
        };
        $scope.menu = [];

        for (var i in permissions) {
            var permission = permissions[i];
            var menuProperties = menuInnerProperties[permission.name];
            if(!menuProperties){
                $log.error("El modulo " + permission.name + " no tiene una configuracion de menu")
            }
            $scope.menu.push({
                name : permission.name,
                state : menuProperties ? menuProperties.state : "/",
                icon : menuProperties ? menuProperties.icon : 'info_outline'});

        };
    });
}());