(function(){
    var app  = angular.module('app.user', []);

    
    app.controller('NewUserCtrl',function(){});
    
    app.factory("UserService",function($resource){
        return $resource('api/v1/crud/user/:id',{id:'@_id'},{
            update: {
                method: 'PUT'
            }
        });
    });



        app.controller("UserListCtrl",function($scope,UserService, TableTemplateService,$mdDialog,$localStorage,Permission){
        var filterSearch = ["givenName","familyName","email"];
        var table = TableTemplateService.setup($scope, UserService,filterSearch);
        $scope.userData = $localStorage.user;
        $scope.isAllowed = function(action){
            return Permission.isAllowed("user",action);
        };
        $scope.showForm = function($event, target){
            $mdDialog.show({
                    controller: 'UserFormCtrl',
                    templateUrl: 'app/user/user.form.html',
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose:false,
                    locals :{
                        target : target
                    },
                    fullscreen: true
                })
                .then(function(answer) {
                    table.refresh(true);
                }, function() {
                    console.log("cancel");
                });
        };
        $scope.onEdit = function($event,target){
            $scope.showForm($event,target)
        };
        $scope.onDelete = function($event,target){
            UserService.delete({id: target.id}, function () {
                table.refresh(true)
            })
        };
    });

    app.controller("UserFormCtrl",function($scope,$mdDialog,ArraySimpleFilter,UserService,RoleService,target){
        var isEdition = angular.isDefined(target);
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.submit = function() {
            if(isEdition){
                UserService.update({id:$scope.entity.id},$scope.entity);
            }else{
                UserService.save($scope.entity);

            }
            $mdDialog.hide();
        };
        $scope.misc = {
            roles : []
        };
        $scope.entity = {
            roles : []
        }
        if(target){
            UserService.get({id: target.id},function(data){
                $scope.entity = data;
            });
        }

        RoleService.get({},function(res){
            $scope.misc.roles = res.list;
        });
    });


}());