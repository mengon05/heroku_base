(function(){
    var app  = angular.module('app.login', ['ui.router','ngResource']);


    app.controller('LoginCtrl', function($scope,AuthService,$mdDialog) {
        $scope.data = {};

        $scope.login = function(){
            AuthService.signIn($scope.data.email,$scope.data.password);
        }
        $scope.passwordRecovery = function($event){
            console.log(1);
            $mdDialog.show({
                    controller: 'PasswordCtrl',
                    templateUrl: 'app/user/password.recovery.html',
                    parent: angular.element(document.body),
                    targetEvent: $event,
                    clickOutsideToClose:false,
                    fullscreen: true
                })
                .then(function(answer) {
                   // table.refresh(true);
                }, function() {
                    console.log("cancel");
                });
        };
    });

    app.controller("PasswordCtrl",function($scope,PasswordService,$mdDialog){
        $scope.recover = function(){
            PasswordService.passwordRecovery($scope.email);
            $mdDialog.hide();
        }
        $scope.cancel = function(){
            $mdDialog.hide();
        }
    });
    app.config(function($stateProvider,$urlRouterProvider) {
        $stateProvider.state('signIn',{
            url:'/signIn',
            templateUrl:'app/login/login.html',
            controller : "LoginCtrl"
        });
    });

    app.factory("Permission", function($localStorage) {
        return {
            isAllowed : function(module,action){
                var excludedActions = $localStorage.user.excludedActions;
                return excludedActions[module + "." + action] == undefined;
            }
        }
    });
    app.factory("PasswordService",function($http,$mdToast,$filter){
        return {
            passwordRecovery : function(email){
                console.log(email);
                return $http({
                    method : "GET",
                    url : "api/v1/user/passwordRecovery",
                    params : {email : email}
                }).then(function(e){
                    console.log(1);
                    var toast = $mdToast.simple();
                    toast.textContent($filter('translate')('view.login.msg.passwordSent'));
                    $mdToast.show(toast);
                })
            }
        }
    })
    app.factory("AuthService",function($resource,$state,$localStorage,$mdToast,$filter,appSettings){
        var res = $resource('api/v1/auth/signIn');

        return {
            signIn : function(email,password){
                var credentials = {email : email,password:  password,clientId : appSettings.clientId};
                res.save(credentials,
                    function success(data, headers ) {
                        var excludedActions = {};
                        for(var i in data.permissions){
                            var permission = data.permissions[i];
                            for(var j in permission.excludedActions){
                                excludedActions[permission.name + "." + permission.excludedActions[j]] = false;
                            }
                        }
                        data.excludedActions = excludedActions;
                        $localStorage.user = data;
                        $state.go("app.userList");
                    },
                    function error(data, headers){
                        var toast = $mdToast.simple();
                        toast.textContent($filter('translate')('error.auth.invalidCredentials'));
                        $mdToast.show(toast);
                    });
            },
            logout : function(){
                $localStorage.user = undefined;
                $state.go("signIn")
            }
        }
    });

    app.factory('AuthInterceptor', ['$localStorage','$injector','$q',function ($localStorage,$injector,$q) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                var authData = $localStorage.user;
                if (authData && authData.auth) {
                    config.headers.Authorization =  authData.auth.token;
                }
                return config;
            },
            responseError: function (response) {
                var $mdToast = $injector.get('$mdToast');
                var $filter = $injector.get('$filter');
                var toast = $mdToast.simple();
                if (response.status === 401) {
                    var authData = $localStorage.user;
                    if(authData && authData.auth) {
                        var AuthService = $injector.get('AuthService');
                        AuthService.logout();
                        toast.textContent($filter('translate')('error.server.httpCode.401'));
                        $mdToast.show(toast);
                    }
                }else if (response.status === 403) {
                    var AuthService = $injector.get('AuthService');
                    AuthService.logout();
                    toast.textContent($filter('translate')('error.server.httpCode.403'));
                    $mdToast.show(toast);
                }
                else if(response.status === 400 && response.data && response.data.errors){
                    if(response.config.defaultErrorHandler === undefined || response.config.defaultErrorHandler){
                        var $translate = $injector.get('$translate');
                        var error = response.data.errors[0];
                        toast.textContent($filter('translate')('error.' + error.module + "." +error.field +"." + error.message));
                        $mdToast.show(toast);
                    }
                    console.log(response);
                }else if(response.status === 500){
                    toast.textContent($filter('translate')('error.server.httpCode.500'));
                    $mdToast.show(toast);
                }

                return  $q.reject(response);
            }
        };
    }]);

    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    });


}());